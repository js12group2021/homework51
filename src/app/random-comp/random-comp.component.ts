import {Component} from '@angular/core';

@Component({
  selector: 'app-random-comp',
  templateUrl: './random-comp.component.html',
  styleUrls: ['./random-comp.component.css']
})
export class RandomCompComponent {

  btnVal:number = 0;
  numbers:number[] = [1,2,3,4,5];
  constructor() {
    this.numbers[0] = this.generateNum();
    this.numbers[1] = this.generateNum();
  }

  generateNum(){
    return  Math.floor((Math.random() * 36) + 5);
  }


  btnClick(event:Event){
    this.btnVal = this.generateNum();
  }

}
